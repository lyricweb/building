# образ за основу, node:12
FROM node:12 as build-stage

# параметр для выбора сборки
ARG BUILD_MODE

# создание директории приложения
WORKDIR /usr/src/app

# символ астериск ("*") используется для того чтобы по возможности
# скопировать оба файла: package.json и package-lock.json
COPY package*.json ./

# установка зависимостей
RUN npm install

# копируем исходный код
COPY . .

# сборка клиента в трех режимах
RUN echo "$BUILD_MODE" \
    && npm run \
        $(if [ "$BUILD_MODE" = 'prod' ]; then echo 'build'; else if [ "$BUILD_MODE" = 'stage' ]; then echo 'build-stage'; else echo 'build'; fi; fi)

# второй этап создания образа
FROM nginx:alpine as production-stage

# копирование dist-itt
COPY --from=build-stage /usr/src/app/dist /usr/share/nginx/html

# открытие 80го порта в контейнере
EXPOSE 80

# запуск nginx
CMD ["nginx", "-g", "daemon off;"]
